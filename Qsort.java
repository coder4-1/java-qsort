import java.util.ArrayList;

public class Qsort {

	private ArrayList<Integer> arrayToSort = new ArrayList<Integer>();

	public Qsort() {
		for(int i=0; i<20; i++) {
		       	arrayToSort.add((int) (Math.random() * 50) + 1);
        	}
	}

	public void execQsort() {
		printArray();

		this.arrayToSort = qsort(this.arrayToSort);

		printArray();
	}

	public void printArray() {
		System.out.println("-------------------------------------------------");
		System.out.println(arrayToSort);
		System.out.println("-------------------------------------------------");
		System.out.println();
	}

	public ArrayList<Integer> qsort(ArrayList<Integer> array) {
		//Basisfall abgehandelt
		if (array.size() == 0 || array.size() == 1) {
			return array;
		}

		//Rekursionsfall array.size() >= 2
		ArrayList<Integer> newArray = new ArrayList<Integer>();
		Integer pivot = array.get(0);
		array.remove(pivot);
		ArrayList<Integer> lower = new ArrayList<Integer>();
		ArrayList<Integer> higher = new ArrayList<Integer>();
		for(Integer curInt : array) {
			if (curInt < pivot) {
				lower.add(curInt);
			} else if (curInt >= pivot) {
				higher.add(curInt);
			}
		}
		lower = qsort(lower);
		higher = qsort(higher);
		for(Integer curInt : lower) {
			newArray.add(curInt);
		}
		newArray.add(pivot);
		for(Integer curInt : higher) {
			newArray.add(curInt);
		}
		return newArray;
	}

}
